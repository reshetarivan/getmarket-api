## Stage 1 - the build process
FROM node:12.13.1 as build-deps
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm i
COPY . ./
EXPOSE 80 443
CMD ["npm", "start"]
