const getItem = (req, res, db) => {
  let { item_name } = req.params;
  item_name = item_name.toLowerCase();
  db.select('items.*', 'categories.name as category')
    .from('items')
    .innerJoin('categories', 'items.cat_id', 'categories.id')
    .where('items.name', 'like', `${item_name}%`)
    .orWhere('items.name','like', `%${item_name}%` )
    .orWhere('items.name','like', `%${item_name}` )
    .orderBy('items.name','ASC')
    .then(items => {
      if (items.length) {
        res.json(items)
      } else {
        res.json([])
      }
    })
    .catch(err => res.status(400).json(err))
};


const getItems = (req, res, db) => {
  let {
    search,
    type,
    category,
    lat,
    lng,
    fromPrice,
    toPrice,
    ne_lat,
    ne_lng,
    sw_lat,
    sw_lng,
    start = 0
  } = req.query;

  fromPrice = parseFloat(fromPrice);
  toPrice = parseFloat(toPrice);

  if (search) { search = search.replace('+', ' ').toLowerCase(); }
  if (category) { category = category.replace('+', ' ').toLowerCase(); }

  console.log('latitude', lat);
  console.log('longitude', lng);
  console.log('type', type);
  console.log('fromPrice', fromPrice);
  console.log('toPrice', toPrice);

  const subquerySearch = db
    .select('items.name')
    .from('items')
    //.where('items.name','like', `${search}`)
    .orWhere('items.name','like', `${search}%`)
    .orWhere('items.name','like', `%${search}%`)
    .orWhere('items.name','like', `%${search}`)
  ;

  function ifType(type) {
    if (!type) return {};
    return {'items.type': type}
  }

  function ifCategory(category) {
    if (!category) return {};
    return {'categories.name': category}
  }

  let toPriceOperator = '>=';
  if (toPrice) {
    toPriceOperator = '<=';
  }

  db
    .select(
      'items.id',
      'items.name',
      'items.descr',
      'items.type',
      'items.price',
      'items.quantity',
      'items.discount',
      // 'shops.id AS shopId',
      // 'shops.lng AS shopLng',
      // 'shops.lat AS shopLat',
      // 'shops.name AS shopName',
      // 'shops.img  AS shopImg',
      // 'shops.addr AS shopAddr',
      // 'shops.zipcode AS shopZipCode',
      // 'shops.rating AS shopRating',
      // 'shops.reviews AS shopReviews',
      // 'users.rating AS userRating',
      // 'users.reviews AS userReviews',
      'categories.name AS catName',
      'cities.name AS cityName',
      'regions.name AS regionName',
      'media.name AS mediaName',
      'media.type AS mediaType',
      'media.order AS mediaOrder',
    )
    .from('items')
    .leftJoin('shops', 'shops.id', 'items.shop_id')
    // .innerJoin('users', 'users.id', 'items.user_id')
    .leftJoin('categories', 'categories.id', 'items.cat_id')
    .leftJoin('cities', 'cities.id', 'shops.city_id')
    .leftJoin('regions', 'regions.id', 'cities.region_id')
    .leftJoin('media', 'media.item_id', 'items.id')
    .whereIn('items.name', subquerySearch )
    // .where('pins.price','>=', fromPrice )
    // .where('pins.price', toPriceOperator, toPrice )
    // .where(ifType(type))
    // .where(ifCategory(category))
    // .where('shops.lat', '<', ne_lat)
    // .where('shops.lat', '>', sw_lat)
    // .where('shops.lng', '<', ne_lng)
    // .where('shops.lng', '>', sw_lng)
    .orderBy('items.name','ASC')
    .then(items => {
      if (items.length) {
        let groupedItems = {
          start,
          data: []
        };

        items.map(pin => {
          const issetItem = groupedItems.data.find(grPin => grPin && grPin.id === pin.id);
          if (!issetItem) {
            let mediaData = [];
            if (pin.mediaName) {
              mediaData = [{
                mediaName: pin.mediaName,
                mediaType: pin.mediaType,
                mediaOrder: pin.mediaOrder,
              }];
            }
            delete pin.mediaName;
            delete pin.mediaType;
            delete pin.mediaOrder;
            groupedItems.data.push({
              ...pin,
              media: mediaData
            });

          } else {
            const idx = groupedItems.data.indexOf(issetItem);
            const issetMedia = groupedItems.data[idx].media.find(m => m.mediaName === pin.mediaName);
            if (!issetMedia) {
              groupedItems.data[idx].media.push({
                mediaName: pin.mediaName,
                mediaType: pin.mediaType,
                mediaOrder: pin.mediaOrder,
              });
              groupedItems.data[idx].media.sort((a,b) => a.mediaOrder - b.mediaOrder)
            }
          }
        });
        groupedItems.total = groupedItems.data.length;

        res.json(groupedItems);
      } else {
        res.json([])
      }
    })
    .catch(err => res.status(400).json(`Error: ${err.message}`))
};

module.exports = {
  getItem,
  getItems
};
