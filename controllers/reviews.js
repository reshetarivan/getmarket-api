const { validationResult } = require('express-validator');
const { _ } = require('lodash');

const getReviews = (req, res, db) => {
  const { shopId } = req.params;
  if (shopId) {
    db
      .select('reviews.id','reviews.user_id','reviews.rate','reviews.review','reviews.date','users.name')
      .from('reviews')
      .innerJoin('users', 'users.id', 'reviews.user_id')
      .where({'reviews.shop_id': parseInt(shopId) })
      .orderBy('reviews.id','DESC')
      .then(reviews => {
        if (reviews.length) {
          const filteredReview = reviews.map(r => {
            const find = ['&quot;', '&#x27;', '&#x2F;', '&#x5C;', '&#96;', '&amp;', '&lt;', '&gt;'];
            const replace = ['"', '\'', '/', '\\', '`', '&', '<', '>'];
            const clearReview = find.reduce((acc, item, i) => {
              const regex = new RegExp(item, "g");
              return acc.replace(regex, replace[i]);
            }, r.review);
            return {...r, review: clearReview};
          });
          res.json({
            data: filteredReview,
            total: filteredReview.length,
            start: 0
          })
        } else {
          res.json({
            data: [],
            total: 0,
            start: 0
          })
        }
      })
      .catch(err => res.status(400).json('Error getting reviews'))
  } else {
    res.status(400).json('Shop ID is required')
  }
};


const postReview = (req, res, db) => {
  const { shop_id, user_id, rate, review, date } = req.body;
  console.log({shop_id});

  const errors = validationResult(req);
  console.log('errors insert', errors.errors);

  if (rate && shop_id) {
    db('reviews').insert({
      shop_id,
      user_id,
      rate,
      review,
      date
    }, 'id')
    .then(review => res.json({
      data: review
    }))
    .catch(err => res.status(400).json(err.detail))
  }
};


const updateReview = (req, res, db) => {
  const { id, shop_id, user_id, rate, review, date } = req.body;
  console.log({shop_id});

  const errors = validationResult(req);
  console.log('errors update', errors.errors);

  if (id  && rate && shop_id) {
    db('reviews')
      .where({'reviews.id': parseInt(id) })
      .update({
        shop_id,
        user_id,
        rate,
        review,
        date
      }, 'id')
      .then(review => {
        // Get total rate and number of reviews
        db('reviews')
          .count('id as totNum')
          .sum('rate as totRate')
          .from('reviews')
          .where({'reviews.shop_id': shop_id })
          .then(rev=> {
            // Update number of reviews and rating for the shop
            db('shops')
              .where({'id': parseInt(shop_id) })
              .update({
                rating: _.floor(rev[0].totRate / rev[0].totNum, 1),
                reviews: parseInt(rev[0].totNum),
              }, 'id')
              .then(shop => res.json({
                id: shop_id,
                rating: _.floor(rev[0].totRate / rev[0].totNum, 1),
                reviews: parseInt(rev[0].totNum)
              }))
          })
      })
      .catch(err => res.status(400).json(err.detail))
  }
};

module.exports = {
  getReviews,
  postReview,
  updateReview
};
