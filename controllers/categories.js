const getCategories = (req, res, db) => {
  const { item_name } = req.params;
  console.log('item_name', item_name);
  if (item_name) {
    db
      .select('categories.id','categories.name')
      .from('categories')
      .innerJoin('items', 'items.category_id', 'categories.id')
      .where('items.name','like', `${item_name}%` )
      .orWhere('items.name','like', `%${item_name}%` )
      .orWhere('items.name','like', `%${item_name}` )
      .distinct('categories.name')
      .orderBy('categories.name','ASC')
      .then(categories => {
        if (categories.length) {
          res.json(categories)
        } else {
          res.json([])
        }
      })
      .catch(err => res.status(400).json('Error getting categories', err))
  } else {
    db
      .select('categories.id','categories.name')
      .from('categories')
      // .innerJoin('items', 'items.category_id', 'categories.id')
      .distinct('categories.name')
      .orderBy('categories.name','ASC')
      .then(categories => {
        if (categories.length) {
          res.json(categories)
        } else {
          res.json([])
        }
      })
      .catch(err => res.status(400).json('Error getting categories', err))
  }

};

module.exports = {
  getCategories
};
