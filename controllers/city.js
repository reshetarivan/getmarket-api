const getCity = (req, res, db) => {
  const { city_name } = req.params;
  db
    .select('cities.name as city', 'regions.name as region', 'countries.name as country')
    .from('cities')
    .innerJoin('regions', 'cities.region_id', 'regions.id')
    .innerJoin('countries', 'regions.country_id', 'countries.id')
    .where({'cities.name': city_name })
    .orderBy('cities.name','ASC')
    .then(cities => {
      if (cities.length) {
        res.json(cities)
      } else {
        res.json('Not found')
      }
    })
    .catch(err => res.status(400).json('Error getting city'))
};

module.exports = {
  getCity
};
