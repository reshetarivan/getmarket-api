const getCity = (req, res, db) => {
  const { region_name } = req.params;
  db
    .select('*')
    .from('regions')
    .innerJoin('cities', 'cities.region_id', 'regions.id')
    .where({'regions.name': region_name })
    .orderBy('cities.name','ASC')
    .then(cities => {
      if (cities.length) {
        res.json(cities)
      } else {
        res.json('Not found')
      }
    })
    .catch(err => res.status(400).json(`Error getting cities: ${err}`))
};

module.exports = {
  getCity
};
