const axios = require('axios');
const getMap = (req, res, db) => {
  const {
    origin,
    destination,
    mode
  } = req.query;

  axios.get(`https://maps.googleapis.com/maps/api/directions/json?
    origin=${origin}&
    destination=${destination}&
    mode=${mode}&
    key=${process.env.REACT_APP_GOOGLE_MAPS_KEY}`
  )
    .then((response) => {
      if (response.data) {
        console.log('directions', response.data);
        res.json(response.data);
      } else {
        res.json('Not found')
      }
    })
    .catch(err => res.status(400).json('Error getting Google Maps API'));

};

module.exports = {
  getMap
};
