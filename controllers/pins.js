const getPins = (req, res, db) => {
  let {
    search,
    type,
    category,
    lat,
    lng,
    fromPrice,
    toPrice,
    ne_lat,
    ne_lng,
    sw_lat,
    sw_lng,
    start = 0
  } = req.query;

  fromPrice = parseFloat(fromPrice);
  toPrice = parseFloat(toPrice);

  if (search) { search = search.replace('+', ' ').toLowerCase(); }
  if (category) { category = category.replace('+', ' ').toLowerCase(); }

  console.log('latitude', lat);
  console.log('longitude', lng);
  console.log('type', type);
  console.log('fromPrice', fromPrice);
  console.log('toPrice', toPrice);

  const subquerySearch = db
    .select('items.name')
    .from('items')
    //.where('items.name','like', `${search}`)
    .orWhere('items.name','like', `${search}%`)
    .orWhere('items.name','like', `%${search}%`)
    .orWhere('items.name','like', `%${search}`)
  ;

  function ifType(type) {
    if (!type) return {};
    return {'items.type': type}
  }

  function ifCategory(category) {
    if (!category) return {};
    return {'categories.name': category}
  }

  let toPriceOperator = '>=';
  if (toPrice) {
    toPriceOperator = '<=';
  }

  db
    .select(
      'pins.id AS pinId',
      'pins.price AS pinPrice',
      'pins.quantity AS pinQuantity',
      'pins.discount AS pinDiscount',
      'shops.id AS shopId',
      'shops.lng AS shopLng',
      'shops.lat AS shopLat',
      'shops.name AS shopName',
      'shops.img  AS shopImg',
      'shops.addr AS shopAddr',
      'shops.zipcode AS shopZipCode',
      'shops.rating AS shopRating',
      'shops.reviews AS shopReviews',
      'categories.name AS catName',
      'cities.name AS cityName',
      'regions.name AS regionName',
      'items.id AS itemId',
      'items.name AS itemName',
      'items.type AS itemType',
      )
    .from('pins')
    .innerJoin('shops', 'shops.id', 'pins.shop_id')
    .innerJoin('items', 'items.id', 'pins.item_id')
    .innerJoin('categories', 'categories.id', 'items.category_id')
    .innerJoin('cities', 'cities.id', 'shops.city_id')
    .innerJoin('regions', 'regions.id', 'cities.region_id')
    .whereIn('items.name', subquerySearch )
    // .where('pins.price','>=', fromPrice )
    // .where('pins.price', toPriceOperator, toPrice )
    .where(ifType(type))
    .where(ifCategory(category))
    // .where('shops.lat', '<', ne_lat)
    // .where('shops.lat', '>', sw_lat)
    // .where('shops.lng', '<', ne_lng)
    // .where('shops.lng', '>', sw_lng)
    .orderBy('shops.name','ASC')
    .then(pins => {
      // console.log({pins});
      if (pins.length) {
        let groupedPins = {
          start,
          totalItems: pins.length,
          totalShops: 0,
          data: []
        };

        pins.map(pin => {
          const issetShop = groupedPins.data.find(grPin => grPin && grPin.shopId === pin.shopId);
          if (!issetShop) {
            groupedPins.data.push({
              shopId: pin.shopId,
              shopName: pin.shopName,
              shopAddr: pin.shopAddr,
              shopImg: pin.shopImg,
              shopLng: pin.shopLng,
              shopLat: pin.shopLat,
              shopZipCode: pin.shopZipCode,
              shopReviews: pin.shopReviews,
              shopRating: pin.shopRating,
              cityName: pin.cityName,
              regionName: pin.regionName,
              items: [{
                catName: pin.catName,
                itemId: pin.itemId,
                itemName: pin.itemName,
                itemType: pin.itemType,
                pinId: pin.pinId,
                pinPrice: pin.pinPrice,
                pinQuantity: pin.pinQuantity,
                pinDiscount: pin.pinDiscount,
              }]
            });
          } else {
            const idx = groupedPins.data.indexOf(issetShop);
            const issetItem = groupedPins.data[idx].items.find(item => item.pinId === pin.pinId);
            if (!issetItem) {
              groupedPins.data[idx].items.push({
                catName: pin.catName,
                itemId: pin.itemId,
                itemName: pin.itemName,
                itemType: pin.itemType,
                pinId: pin.pinId,
                pinPrice: pin.pinPrice,
              });
            }
          }
        });

        groupedPins.totalShops = groupedPins.data.length;

        res.json(groupedPins)
      } else {
        res.json([])
      }
    })
    .catch(err => res.status(400).json(`Error: ${err.message}`))
};

module.exports = {
  getPins
};
