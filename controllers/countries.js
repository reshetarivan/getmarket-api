const getCountries = (req, res, db) => {

  res.json(process.env);

  db.select('*').from('countries').orderBy('name','ASC')
    .then(countries => {
      if (countries.length) {
        res.json(countries)
      } else {
        res.json('Not found')
      }
    })
    .catch(err => res.status(400).json('Error getting countries'))
};

module.exports = {
  getCountries
};
