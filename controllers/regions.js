const getRegions = (req, res, db) => {
  const { country_name } = req.params;
  db
    .select('*')
    .from('countries')
    .innerJoin('regions', 'countries.id', 'regions.country_id')
    .where({'countries.name': country_name })
    .orderBy('regions.name','ASC')
    .then(regions => {
      if (regions.length) {
        res.json(regions)
      } else {
        res.json('Not found')
      }
    })
    .catch(err => res.status(400).json('Error getting regions'))
};

module.exports = {
  getRegions
};
