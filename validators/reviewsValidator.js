const { check } = require('express-validator');
const maxReviewEnterLength = 1000;

module.exports.validate = (method) => {
  switch (method) {
    case 'postReview': {
      console.log('postReview');
      return [
        check('id', 'ID doesnt exists').escape().trim().exists().isInt(),
        check('shop_id', 'Shop ID doesnt exists').escape().trim().exists().isInt(),
        check('user_id', 'User ID doesnt exists').escape().trim().exists().isInt(),
        check('rate', 'Rate doesnt exists').escape().trim().exists().isInt(),
        check('review').escape().trim().exists().isString().isLength({ max: maxReviewEnterLength }),
        check('date', 'Date doesnt exists').escape().trim().exists().isInt(),
      ]
    }
    default: {
      console.log('default');
      return [];
    }
  }
};
