const express = require('express');
const knex = require('knex');
const bodyParser = require('body-parser');
const cors = require('cors');

const categories = require('./controllers/categories');
const countries = require('./controllers/countries');
const regions = require('./controllers/regions');
const cities = require('./controllers/cities');
const city = require('./controllers/city');
const items = require('./controllers/items');
const pins = require('./controllers/pins');
const gmap = require('./controllers/gmap');
const reviews = require('./controllers/reviews');


const reviewsValidator = require('./validators/reviewsValidator');


const port = process.env.DOMAIN_PORT || 4000;
const app = express();

const db = knex({
  client: 'mysql',
  version: '8.0',
  connection: {
    host : process.env.DB_HOST,
    user : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    database : process.env.DB_NAME
  }
});
app.use(bodyParser.json());
app.use(cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/categories/:item_name?', (req, res) => {categories.getCategories(req, res, db)});
app.get('/countries', (req, res) => {countries.getCountries(req, res, db)});
app.get('/regions/:country_name', (req, res) => {regions.getRegions(req, res, db)});
app.get('/cities/:region_name', (req, res) => {cities.getCity(req, res, db)});
app.get('/city/:city_name', (req, res) => {city.getCity(req, res, db)});
app.get('/items', (req, res) => {items.getItems(req, res, db)});
app.get('/items/:item_name', (req, res) => {items.getItem(req, res, db)});
app.get('/pins', (req, res) => {pins.getPins(req, res, db)});
app.get('/gmap', (req, res) => {gmap.getMap(req, res, db)});
app.get('/reviews/:shopId', (req, res) => {reviews.getReviews(req, res, db)});
app.post('/reviews', reviewsValidator.validate('postReview'), (req, res) => {reviews.postReview(req, res, db)});
app.post('/reviews/:reviewId', reviewsValidator.validate('postReview'), (req, res) => {reviews.updateReview(req, res, db)});


app.listen(port, ()=> {
  console.log('app is running on port ' + port);
});
